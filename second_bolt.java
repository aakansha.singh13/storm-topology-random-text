import java.util.HashMap;
import java.util.Map;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.IRichBolt;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Tuple;

public class second_bolt implements IRichBolt
{
    Map<String, Integer> counterMap;
    private OutputCollector collector;

    @Override
    public void prepare(Map conf, TopologyContext context, OutputCollector collector)
    {
        this.counterMap = new HashMap<String, Integer>();
        this.collector = collector;
    }

    @Override
    public void execute(Tuple tuple)
    {
        String text = tuple.getString(0);
        if(!counterMap.containsKey(text))
        {
            counterMap.put(text,1);
        }
        else
        {
            Integer c = counterMap.get(text) + 1;
            counterMap.put(text, c);
        }
        collector.ack(tuple);
    }
    @Override
    public void cleanup()
    {
        for(Map.Entry<String, Integer> entry:counterMap.entrySet())
        {
            System.out.println(entry.getKey()+":"+enrty.getValue());
        }

    }
    @Override
    public void DeclareOutputFields(OutputFieldsDeclarer declarer)
    {
        declarer.declare(new Fields("text"));
    }
    @Override
    public Map<String, Object> getComponentConfiguration()
    {
        return null;
    }

}