import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.topology.TopologyBuilder;

public class topology
{
    public static void main(String args[])throws IOException
    {
        Config config = new Config();
        config.setDebug(true);
        TopologyBuilder builder = new TopologyBuilder();
        builder.setSpout("random-text-generator", new spout_trial());
        builder.setBolt("read-random-text", new bolt()).shuffleGrouping("random-text-generator");
        builder.setBolt("count-random-text", new second_bolt()).fieldsGrouping("random-text-generator", new Fields("text"));
        LocalCluster cluster = new LocalCluster();
        cluster.submitTopology("topology", config, builder.createTopology());
        Thread.sleep(10000);
        cluster.shutdown();
    }

}
