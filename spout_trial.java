import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.IRichSpout;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import java.util.*;
// spout tuple packages
// spout interface packages
public class spout_trial implements IRichSpout
{
    // instance for spout collector which passes tuples to bolt
    private SpoutOutputCollector collector;
    private boolean completed = false;
    // instance for TopologyContext which contains topology data
    private TopologyContext context;
    // private Random randomGenerator = new Random();
    private Integer idx = 0;

    @Override
    public void open(Map conf, TopologyContext context, SpoutOutputCollector collector)
    {
        this.context = context;
        this.collector = collector;
    }
    @Override
    public void NextTuple()
    {
        if (this.idx<=1000)
        {
            List<String> randomString = new ArrayList<String>();
            int length = 10;
            boolean useLetters = true;
            boolean useNumbers = false;
            while(this.idx++<1000)
            {
                String generatedString = RandomStringUtils.random(length, useLetters, useNumbers);
                randomString.add(generatedString);
                this.collector.emit(new Values(generatedString));

            }//while

        }//if
    }//NextTuple

    @Override
    public void DeclareOutputFiels(OutputFieldsDeclarer declarer)
    {
        declarer.declare(new Fields("text"));
    }
    @Override
    public void close()
    {

    }

    public boolean isDistributed()
    {
        return false;
    }

    @Override
    public void activate() 
    {

    }
    @Override
    public void deactivate() 
    {

    }
    @Override
    public void ack(Object msgID)
    {

    }
    @Override
    public void fail(Object msgId)
    {

    }
    @Override
    public Map<String, Object> getComponentConfiguration() 
    {
        return null;
    }

}
