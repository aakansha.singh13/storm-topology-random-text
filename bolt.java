import java.util.HashMap;
import java.util.Map;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.IRichBolt;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Tuple;

public class bolt implements IRichBolt
{
    private OutputCollector collector;

    @Override
    public void prepare(Map conf, TopologyContext context, OutputCollector collector)
    {
        this.collector = collector;
    }
    @Override
    public void execute(Tuple tuple)
    {
        String text = tuple.getString(0);
        collector.emit(new Values(text));
    }
    @Override
    public void cleanup()
    {

    }
    @Override
    public void DeclareOutputFields(OutputFieldsDeclarer declarer)
    {
        declarer.declarer(new Fields("text"));
    }
    @Override
    public Map<String, Object> getComponentConfifguration()
    {
        return null;
    }

}